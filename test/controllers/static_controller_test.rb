require 'test_helper'

class StaticControllerTest < ActionDispatch::IntegrationTest
  test "should get inicio" do
    get static_inicio_url
    assert_response :success
  end

  test "should get precios" do
    get static_precios_url
    assert_response :success
  end

end
