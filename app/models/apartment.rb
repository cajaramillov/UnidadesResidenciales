class Apartment < ApplicationRecord
  belongs_to :building
  has_many :owners
  has_many :people, through: :owners
  validates_presence_of :nombre, message: 'No puede estar en blanco'
  validates :tipo, numericality: {only_integer: true,
                                  greater_than_or_equal_to: 1,
                                  less_than_or_equal_to: 4,
                                  message: 'Debes seleccionarlo correctamente'}

  TIPO = {1 =>'Apartamento', 2=>'Parquedero', 3=>'San Alejo', 4=>'Local'}

  def nombre_tipo
    Apartment::TIPO[self.tipo]
  end
end
