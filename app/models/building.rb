class Building < ApplicationRecord
  belongs_to :user
  has_many :employee
  has_many :apartments
  has_many :people

  validates :nit, presence: {message: 'No puede estar en blanco'}
  validates :nombre, presence: {message: 'No puede estar en blanco'}
  validates_presence_of :telefono, message: 'No puede estar en blanco'

  def nombre_completo
    return "#{nombre}, #{nit}"
  end
end
