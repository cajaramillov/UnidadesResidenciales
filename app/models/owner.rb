class Owner < ApplicationRecord
  belongs_to :person
  belongs_to :apartment
end