class Person < ApplicationRecord
  belongs_to :building
  has_many :owners
  has_many :apartments, through: :owners
end
