class Employee < ApplicationRecord
  belongs_to :building

=begin
  validates :cedula, presence: {message: 'No puede estar en blanco'}
  validates :nombre, presence: {message: 'No puede estar en blanco'}
  validates :email, presence: {message: 'No puede estar en blanco'}
=end

  validates_presence_of :cedula, :nombre, :email, message: 'No puede estar en blanco'
  validates :edad, numericality: {only_integer: true,
                                  greater_than_or_equal_to: 0,
                                  less_than_or_equal_to: 120,
                                  message: 'La edad debe estar entre 0 y 120 años'}
  validates :cargo, presence: {message: 'No puede estar en blanco'}
end
