class EmployeesController < ApplicationController
  before_action :authenticate_user!
  before_action :set_employee, only: [:show, :edit, :update, :destroy]

  # GET /building/:building_id/employees
  # GET /employees.json
  def index
    @building = current_user.buildings.find(params[:building_id])
    @employees = @building.employee.all
  end

  # GET building/:building_id/employees/1
  # GET /employees/1.json
  def show
  end

  # GET /building/:building_id/employees/new
  def new
    @building = current_user.buildings.find(params[:building_id])
    @employee = @building.employee.new
  end

  # GET building/:building_id/employees/1/edit
  def edit
  end

  # POST building/:building_id/employees
  # POST building/:building_id/employees.json
  def create
    @building = current_user.buildings.find(params[:building_id])
    @employee = @building.employee.new(employee_params)

    respond_to do |format|
      if @employee.save
        format.html { redirect_to building_employee_path(@building, @employee), notice: 'El Empleado fue creado satisfactoriamente' }
        format.json { render :show, status: :created, location: @employee }
      else
        format.html { render :new }
        format.json { render json: @employee.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT building/:building_id/employees/1
  # PATCH/PUT building/:building_id/employees/1.json
  def update
    respond_to do |format|
      if @employee.update(employee_params)
        format.html { redirect_to building_employee_path(@building, @employee), notice: 'El Empleado fue Modificado Satisfactoriemnte' }
        format.json { render :show, status: :ok, location: @employee }
      else
        format.html { render :edit }
        format.json { render json: @employee.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE building/:building_id/employees/1
  # DELETE building/:building_id/employees/1.json
  def destroy
    @employee.destroy
    respond_to do |format|
      format.html { redirect_to building_employees_path(@building), notice: 'El Empleado fue borrado satisfactoriamente.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_employee
      @building = current_user.buildings.find(params[:building_id])
      @employee = @building.employee.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def employee_params
      params.require(:employee).permit(:cedula, :nombre, :telefono, :direccion, :cargo, :email, :edad, :fecha_nacimiento)
    end
end
