class ApartmentsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_apartment, only: [:show, :edit, :update, :destroy]

  # GET /apartments
  # GET /apartments.json
  def index
    @building = current_user.buildings.find(params[:building_id])
    @apartments = @building.apartments.all
  end

  # GET /apartments/1
  # GET /apartments/1.json
  def show
  end

  # GET /apartments/new
  def new
    @building = current_user.buildings.find(params[:building_id])
    @apartment = @building.apartments.new
  end

  # GET /apartments/1/edit
  def edit
  end

  # POST /apartments
  # POST /apartments.json
  def create
    @building = current_user.buildings.find(params[:building_id])
    @apartment = @building.apartments.new(apartment_params)

    respond_to do |format|
      if @apartment.save
        format.html { redirect_to building_apartment_path(@building, @apartment), notice: 'El Apartamento fue creado Satisfactoriamente' }
        format.json { render :show, status: :created, location: @apartment }
      else
        format.html { render :new }
        format.json { render json: @apartment.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /apartments/1
  # PATCH/PUT /apartments/1.json
  def update
    respond_to do |format|
      if @apartment.update(apartment_params)
        format.html { redirect_to building_apartment_path(@building, @apartment), notice: 'El Apartamento fue modificado Satisfactoriamente' }
        format.json { render :show, status: :ok, location: @apartment }
      else
        format.html { render :edit }
        format.json { render json: @apartment.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /apartments/1
  # DELETE /apartments/1.json
  def destroy
    @apartment.destroy
    respond_to do |format|
      format.html { redirect_to building_apartments_path(@building), notice: 'El Apartamento fue borrado Satisfactoriamente' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_apartment
      @building = current_user.buildings.find(params[:building_id])
      @apartment = @building.apartments.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def apartment_params
      params.require(:apartment).permit(:nombre, :matricula, :area, :coeficiente, :tipo)
    end
end
