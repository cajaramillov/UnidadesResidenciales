class PeopleController < ApplicationController
  before_action :authenticate_user!
  before_action :set_person, only: [:show, :edit, :update, :destroy]

  # GET /people
  # GET /people.json
  def index
    @building = current_user.buildings.find(params[:building_id])
    @people = @building.people.all
  end

  # GET /people/1
  # GET /people/1.json
  def show
  end

  # GET /people/new
  def new
    @building = current_user.buildings.find(params[:building_id])
    @person = @building.people.new
  end

  # GET /people/1/edit
  def edit
  end

  # POST /people
  # POST /people.json
  def create
    @building = current_user.buildings.find(params[:building_id])
    @person = @building.people.new(person_params)

    respond_to do |format|
      if @person.save
        format.html { redirect_to building_person_path(@building, @person), notice: 'La Persona fue adicionada Satisfactoriamente' }
        format.json { render :show, status: :created, location: @person }
      else
        format.html { render :new }
        format.json { render json: @person.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /people/1
  # PATCH/PUT /people/1.json
  def update
    respond_to do |format|
      if @person.update(person_params)
        format.html { redirect_to building_person_path(@building, @person), notice: 'La Persona fue modificada Satisfactoriamente' }
        format.json { render :show, status: :ok, location: @person }
      else
        format.html { render :edit }
        format.json { render json: @person.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /people/1
  # DELETE /people/1.json
  def destroy
    @person.destroy
    respond_to do |format|
      format.html { redirect_to building_people_path(@building), notice: 'Person was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_person
      @building = current_user.buildings.find(params[:building_id])
      @person = @building.people.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def person_params
      params.require(:person).permit(:nombre, :cedula, :telefono, :correo, :direccion)
    end
end
