class OwnersController < ApplicationController
  before_action :authenticate_user!
  before_action :set_owner, only: [:show, :edit, :update, :destroy]

  # GET /owners
  # GET /owners.json
  def index
    @building = current_user.buildings.find(params[:building_id])
    @apartment = @building.apartments.find(params[:apartment_id])
    @owners = @apartment.owners.all
  end

  # GET /owners/1
  # GET /owners/1.json
  def show
  end

  # GET /owners/new
  def new
    @building = current_user.buildings.find(params[:building_id])
    @apartment = @building.apartments.find(params[:apartment_id])
    @owner = @apartment.owners.new
  end

  # GET /owners/1/edit
  def edit
  end

  # POST /owners
  # POST /owners.json
  def create
    @building = current_user.buildings.find(params[:building_id])
    @apartment = @building.apartments.find(params[:apartment_id])
    @owner = @apartment.owners.new(owner_params)

    respond_to do |format|
      if @owner.save
        format.html { redirect_to building_apartment_owner_path(@building, @apartment, @owner), notice: 'El propietario fue adicionado satisfactoriamente' }
        format.json { render :show, status: :created, location: @owner }
      else
        format.html { render :new }
        format.json { render json: @owner.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /owners/1
  # PATCH/PUT /owners/1.json
  def update
    respond_to do |format|
      if @owner.update(owner_params)
        format.html { redirect_to building_apartment_owner_path(@building, @apartment, @owner), notice: 'El propietario fue modificado satisfactoriamente.' }
        format.json { render :show, status: :ok, location: @owner }
      else
        format.html { render :edit }
        format.json { render json: @owner.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /owners/1
  # DELETE /owners/1.json
  def destroy
    @owner.destroy
    respond_to do |format|
      format.html { redirect_to building_apartment_owners_path(@building, @apartment), notice: 'El propietario fue borrado satisfactoriamente' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_owner
      @building = current_user.buildings.find(params[:building_id])
      @apartment = @building.apartments.find(params[:apartment_id])
      @owner = @apartment.owners.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def owner_params
      params.require(:owner).permit(:person_id, :fecha_inicio, :fecha_fin, :activo)
    end
end
