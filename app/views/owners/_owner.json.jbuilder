json.extract! owner, :id, :person_id, :apartment_id, :fecha_inicio, :fecha_fin, :activo, :created_at, :updated_at
json.url owner_url(owner, format: :json)
