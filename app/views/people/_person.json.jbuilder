json.extract! person, :id, :nombre, :cedula, :telefono, :correo, :direccion, :building_id, :created_at, :updated_at
json.url person_url(person, format: :json)
