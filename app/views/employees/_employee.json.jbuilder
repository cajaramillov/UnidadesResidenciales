json.extract! employee, :id, :cedula, :nombre, :telefono, :direccion, :cargo, :email, :edad, :fecha_nacimiento, :building_id, :created_at, :updated_at
json.url employee_url(employee, format: :json)
