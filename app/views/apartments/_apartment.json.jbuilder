json.extract! apartment, :id, :nombre, :matricula, :area, :coeficiente, :tipo, :building_id, :created_at, :updated_at
json.url apartment_url(apartment, format: :json)
