class ApplicationMailer < ActionMailer::Base
  default from: 'info@sandboxfa1a5112327149899d1571ed7b6d81ee.mailgun.org'
  layout 'mailer'
end
