class CreateApartments < ActiveRecord::Migration[5.0]
  def change
    create_table :apartments do |t|
      t.string :nombre
      t.string :matricula
      t.float :area
      t.float :coeficiente
      t.integer :tipo
      t.references :building, foreign_key: true

      t.timestamps
    end
  end
end
