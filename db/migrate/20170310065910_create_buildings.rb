class CreateBuildings < ActiveRecord::Migration[5.0]
  def change
    create_table :buildings do |t|
      t.string :nit, null: false
      t.string :nombre, null: false
      t.string :telefono
      t.string :direccion
      t.boolean :activo
      t.text :historia

      t.timestamps
    end
  end
end
