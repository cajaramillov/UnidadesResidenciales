class CreatePeople < ActiveRecord::Migration[5.0]
  def change
    create_table :people do |t|
      t.string :nombre
      t.string :cedula
      t.string :telefono
      t.string :correo
      t.string :direccion
      t.references :building, foreign_key: true

      t.timestamps
    end
  end
end
