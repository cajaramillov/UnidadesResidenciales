class CreateOwners < ActiveRecord::Migration[5.0]
  def change
    create_table :owners do |t|
      t.references :person, foreign_key: true
      t.references :apartment, foreign_key: true
      t.date :fecha_inicio
      t.date :fecha_fin
      t.boolean :activo

      t.timestamps
    end
  end
end
