Rails.application.routes.draw do
  get 'static/inicio'

  get 'static/precios'

  devise_for :users
  resources :buildings do
    resources :employees
    resources :apartments do
      resources :owners
    end
    resources :people
  end

  root 'static#inicio' #Siempre debe ir de ultimo
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
